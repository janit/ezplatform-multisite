<?php

namespace MainsitesBundle\Listener;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class HeadersListener {

    public function onKernelResponse(FilterResponseEvent $event){

        $responseHeaders = $event->getResponse()->headers;

        $responseHeaders->set('foo','bar');

    }
}