<?php

namespace MainsitesBundle\QueryType;

use eZ\Publish\Core\QueryType\QueryType;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query;

class BlogPostsQueryType implements QueryType {

    public function getQuery(array $parameters = [])
    {

        $filter = new Query\Criterion\LogicalAnd([
            new Query\Criterion\ContentTypeIdentifier('blog_posts'),
        ]);

        return new LocationQuery([
            'filter' => $filter,
        ]);

    }

    public function getSupportedParameters()
    {
        return ['parent_path'];
    }

    public static function getName()
    {
        return 'BlogPostsQueryType';
    }


}