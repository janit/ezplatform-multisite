<?php

namespace MainsitesBundle\Controller;

use eZ\Publish\Core\MVC\Symfony\View\ContentView;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContentController extends Controller
{
    public function landingpageAction(ContentView $view, Request $request)
    {

        $view->addParameters(['random', mt_rand(1,1000)]);

        return $view;

    }
}
